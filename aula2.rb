Class Usuario   #declarando classe usuario e seus atributos
    attr accessor :email, :senha, :nome, :nascimento, :logado

    def initialize (email, senha, nome, nascimento, logado)
        @email = email
        @senha = senha
        @nome   = nome
        @nascimento  = nascimento
        @logado = true
    end

                    #definindo se a senha digitada for igual a @senha, vai logar ou não
    def logar     
        puts "Digite sua senha:"        
        inserir_senha = gets.to_s         #transforma a senha insedira toda em string para comparar se está certa ou não
        if inserir_senha = @senha
            @logado = true      
        else
            @logado = false        
    end

        def deslogar
            @logado = false
        end



        def idade
            puts "digite o dia do seu nascimento:"
            puts "digite o mes do seu nascimento:"
            puts "digite o ano do seu nascimento:"
            ano_nascimento == gets.to_i                         #pega o ano de nascimento informado e transforma em um numero inteiro para realizar a conta da idade
            nascimento = 2020 - ano_nascimento
            puts nascimento + "anos"
        end

end


Class Aluno         # declarando classe aluno e seus atributos 
    attr_accessor :matricula, :periodo, :curso, :turmas

    def initialize (matricula, periodo, curso, turmas)
        @matricula = matricula
        @periodo = periodo
        @curso =  curso
        @turmas = []

    end

    def inscrever(turma_nome)   #declarando variavel que adiciona o nome da turma a array do atributo 
        @turmas.push(turma_nome)
    end




Clsss Materia          # declarando classe materia e seus atributos 
    attr_accessor :ementa, :nome, :professor

    def initialize (ementa, nome, professor)
        @ementa = ementa
        @nome = nome
        @professor = []
    end

    def incluir_professor(professor_nome)            #declarando variavel que adiciona o nome do professor ao array do atributo 
        @professor.push(professor_nome)
    end




Class Professor           # declarando classe professor e seus atributos 
    attr_accessor :matricula, :salario, :materia

    def initialize (matricula, salario, materia)
        @matricula = matricula
        @salario = salario.to_f
        @materia = []
    end

    def incluir_materia(materia_nome)             #declarando variavel que adiciona o nome da materia a array do atributo 
        @materia.push(materia_nome)
    end




Class Turma               #declarando classe turma e seus atributos 
    attr_accessor :nome, :horario, :dias, :inscritos, :inscricao_aberta

    def initialize(nome, horario, dias, inscritos, inscricao_aberta)
        @nome = nome
        @horario = horario
        @dias = []
        @inscritos = []
        @inscricao_aberta = true
    end

    def abrir_inscricao
        puts "essa turma está recebendo inscrição?"         #pede uma declaração ao adm se a turma estará disponivel para inscrição em tal periodo.
        insc_aberta = gets.to_s
        if insc_aberta == "sim"                      # se o adm responder que a turma esta rebendo inscrições então, o atributo de inscrições ao aluno retorna ao usuario que é posivel se inscrever
            @inscricao_aberta = true
            puts "materia aberta para inscrição"
        else
            @inscricao_aberta = false                 # se o adm responder que a turma nao esta aberta para inscrição, aparece para o usuario que a turma está fechada
            puts "inscrições fechadas"
        end

    def adicionar_dias(dia_da_semana)         #  declarando variavel que adiciona os dias da semana ao array do atributo 
        @dias.push(dia_da_semana)
    end

    def adicionar_inscritos(alunx)       # declarando variavel que adiciona o nome do aluno inscrito ao array do atributo 
        @inscritos.push(alunx)
    end



end
